﻿%namespace LexScanner
%using System.Text.RegularExpressions;
%visibility internal
%option verbose


%x PHRASE

alpha [a-zA-Z]
%%
; { return (int)Tokens.OR; }

, { return (int)Tokens.AND; }

{alpha} {
  str.Clear();
  str.Append(yytext);
  BEGIN(PHRASE);
}

<PHRASE> {
  \s* { }
  {alpha}+ {
    str.Append(yytext + " ");
    if (!Char.IsLetter((char) this.code)
      && !Char.IsWhiteSpace((char) this.code)) {
      BEGIN(INITIAL);
      yylval = new Node();
      yylval.sVal = str.ToString().Trim();
      yylval.Type = NodeType.VALUE;
      return (int)Tokens.WORDS; 
    }
  }
}

[\n\r]* {}

\.      { return (int)Tokens.DOT; }
%%
StringBuilder str = new StringBuilder();
internal Scanner(String source) {
  this.SetSource(source, 0);
}
