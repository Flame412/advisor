%namespace LexScanner
%visibility internal
%output=Parser.cs
%YYSTYPE LexScanner.Node

%token DOT
%token <sVal> WORDS
%start Start

%left OR AND

%%
Start : 
    StatementList {
        this.Root = $1;
    }
    ;

Statement:
    Expression DOT {
        $$ = $1;
    }
    ;

StatementList:
    Statement StatementList {
        Node node = new Node($1, $2);
        $$ = node;
    }
    | /*empty*/ {
        $$ = null;
    }
    ;

Expression:
    WORDS AND Expression {
        Node list = new Node(null,null);
        list.sVal = $1;
        list.Type = NodeType.VALUE;
        Node node = new Node(list,$3);
        node.Type = NodeType.AND;
        $$ = node;
    }
    | WORDS OR Expression {
        Node list = new Node(null,null);
        list.sVal = $1;
        list.Type = NodeType.VALUE;
        Node node = new Node(list,$3);
        node.Type = NodeType.OR;
        $$ = node;
    }
    | WORDS {
        Node list = new Node(null,null);
        list.sVal = $1;
        list.Type = NodeType.VALUE;
        $$ = list;
    }
    ;

%%
private Node _root;
internal Node Root {
    get { return _root; }
    set { _root = value; }    
}

internal Parser(Scanner lex) : base(lex) {
}
