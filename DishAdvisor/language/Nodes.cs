using System;

namespace LexScanner {
	public enum NodeType {
		NONE = 0,
		AND = 1,
		OR = 2,
		VALUE = 3
	}
	
    public class Node {
        private Node _left, _right;
        private String _sVal;
        private NodeType _type;
        
        #region get/set
		public string sVal {
			get { return _sVal; }
			set { _sVal = value; }
		}
        
		public NodeType Type {
			get { return _type; }
			set { _type = value; }
		}
        
		public Node Right {
			get { return _right; }
		}
        
		public Node Left {
			get { return _left; }
		}
        #endregion

        public Node(Node left, Node right) {
        	_left = left;
        	_right = right;
        }
        
        public Node() {
        }
    }
}
