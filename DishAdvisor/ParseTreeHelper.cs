﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Tursukov A.E.
 * Дата: 08.06.2015
 * Время: 16:16
 * 
 */
using System;
using System.Collections.Generic;
using LexScanner;

namespace DishAdvisor
{
	/// <summary>
	/// Description of ParseTreeHelper.
	/// </summary>
	public static class ParseTreeHelper
	{
		
		public static void ParseNode(Node node, ref QueryPool pool) {
			switch (node.Type) {
				case NodeType.NONE:
					if (node.sVal != null) { //FIXME: this is a crutch
						pool.Add(node.sVal);
					}
					if (node.Left != null) { 
						ParseNode(node.Left, ref pool); 
					}
					if (node.Right != null) {
						ParseNode(node.Right, ref pool);
					}
					break;
				case NodeType.AND:
					pool.Add(node.Left.sVal);
					ParseNode(node.Right, ref pool);
					break;
				case NodeType.OR:
					pool.AddDoublingWithDelay(node.Left.sVal);
					if (node.Right.Type != NodeType.OR) {
						if (node.Right.Type == NodeType.VALUE) {
							pool.AddDoublingWithDelay(node.Right.sVal);
						} else if (node.Right.Type == NodeType.AND) {
							pool.AddDoublingWithDelay(node.Right.Left.sVal);
						}
						pool.ResolveDelayedDoubling();
					}
					ParseNode(node.Right, ref pool);
					break;
				default:
					throw new Exception("Invalid value for NodeType");
			}
		}
		
		public static List<QueryPool> ParseTree(Node root) {
			List<QueryPool> result = new List<QueryPool>();
			
			if (root != null) {
				if (root.Left != null) {
					QueryPool pool = new QueryPool();
					ParseNode(root.Left, ref pool);
					result.Add(pool);
				}
				
				if (root.Right != null) {
					result.AddRange(ParseTree(root.Right));
				}	
			}
			
			return result;
		}
	}
}
