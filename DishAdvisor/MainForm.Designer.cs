﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Tursukov A.E.
 * Дата: 28.05.2015
 * Время: 15:58
 * 
 */
namespace DishAdvisor
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.CreateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.OpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.RedactToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.RedactBaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.RedactViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.AE_listBox_output = new System.Windows.Forms.ListBox();
			this.AE_webBrowser_output = new System.Windows.Forms.WebBrowser();
			this.AE_textBox_consoleInput = new System.Windows.Forms.TextBox();
			this.AE_textBox_consoleOutput = new System.Windows.Forms.TextBox();
			this.menuStrip1.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.FileToolStripMenuItem,
									this.RedactToolStripMenuItem,
									this.AboutToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(792, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// FileToolStripMenuItem
			// 
			this.FileToolStripMenuItem.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuItem;
			this.FileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.CreateToolStripMenuItem,
									this.OpenToolStripMenuItem,
									this.ExitToolStripMenuItem});
			this.FileToolStripMenuItem.Name = "FileToolStripMenuItem";
			this.FileToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
			this.FileToolStripMenuItem.Text = "Файл";
			// 
			// CreateToolStripMenuItem
			// 
			this.CreateToolStripMenuItem.Name = "CreateToolStripMenuItem";
			this.CreateToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
			this.CreateToolStripMenuItem.Text = "Создать";
			// 
			// OpenToolStripMenuItem
			// 
			this.OpenToolStripMenuItem.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuItem;
			this.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem";
			this.OpenToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
			this.OpenToolStripMenuItem.Text = "Открыть";
			this.OpenToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItemClick);
			// 
			// ExitToolStripMenuItem
			// 
			this.ExitToolStripMenuItem.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuItem;
			this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
			this.ExitToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
			this.ExitToolStripMenuItem.Text = "Выход";
			// 
			// RedactToolStripMenuItem
			// 
			this.RedactToolStripMenuItem.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuItem;
			this.RedactToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.RedactBaseToolStripMenuItem,
									this.RedactViewToolStripMenuItem});
			this.RedactToolStripMenuItem.Name = "RedactToolStripMenuItem";
			this.RedactToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
			this.RedactToolStripMenuItem.Text = "Редактор";
			// 
			// RedactBaseToolStripMenuItem
			// 
			this.RedactBaseToolStripMenuItem.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuItem;
			this.RedactBaseToolStripMenuItem.Name = "RedactBaseToolStripMenuItem";
			this.RedactBaseToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
			this.RedactBaseToolStripMenuItem.Text = "Редактировать базу знаний";
			this.RedactBaseToolStripMenuItem.Click += new System.EventHandler(this.RedactBaseToolStripMenuItemClick);
			// 
			// RedactViewToolStripMenuItem
			// 
			this.RedactViewToolStripMenuItem.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuItem;
			this.RedactViewToolStripMenuItem.Name = "RedactViewToolStripMenuItem";
			this.RedactViewToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
			this.RedactViewToolStripMenuItem.Text = "Редактировать файлы отображения";
			this.RedactViewToolStripMenuItem.Click += new System.EventHandler(this.RedactViewToolStripMenuItemClick);
			// 
			// AboutToolStripMenuItem
			// 
			this.AboutToolStripMenuItem.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuItem;
			this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
			this.AboutToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
			this.AboutToolStripMenuItem.Text = "О программе";
			this.AboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItemClick);
			// 
			// statusStrip1
			// 
			this.statusStrip1.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar;
			this.statusStrip1.Location = new System.Drawing.Point(0, 551);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(792, 22);
			this.statusStrip1.TabIndex = 1;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.splitContainer1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.AE_textBox_consoleInput, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.AE_textBox_consoleOutput, 0, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(5);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(3);
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(792, 527);
			this.tableLayoutPanel1.TabIndex = 2;
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(6, 6);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.AE_listBox_output);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.AE_webBrowser_output);
			this.splitContainer1.Size = new System.Drawing.Size(780, 306);
			this.splitContainer1.SplitterDistance = 222;
			this.splitContainer1.TabIndex = 0;
			// 
			// AE_listBox_output
			// 
			this.AE_listBox_output.Dock = System.Windows.Forms.DockStyle.Fill;
			this.AE_listBox_output.FormattingEnabled = true;
			this.AE_listBox_output.Location = new System.Drawing.Point(0, 0);
			this.AE_listBox_output.Name = "AE_listBox_output";
			this.AE_listBox_output.Size = new System.Drawing.Size(222, 306);
			this.AE_listBox_output.TabIndex = 0;
			this.AE_listBox_output.SelectedIndexChanged += new System.EventHandler(this.AE_listBox_outputSelectedIndexChanged);
			// 
			// AE_webBrowser_output
			// 
			this.AE_webBrowser_output.Dock = System.Windows.Forms.DockStyle.Fill;
			this.AE_webBrowser_output.Location = new System.Drawing.Point(0, 0);
			this.AE_webBrowser_output.MinimumSize = new System.Drawing.Size(20, 20);
			this.AE_webBrowser_output.Name = "AE_webBrowser_output";
			this.AE_webBrowser_output.Size = new System.Drawing.Size(554, 306);
			this.AE_webBrowser_output.TabIndex = 0;
			// 
			// AE_textBox_consoleInput
			// 
			this.AE_textBox_consoleInput.Dock = System.Windows.Forms.DockStyle.Fill;
			this.AE_textBox_consoleInput.Font = new System.Drawing.Font("Consolas", 8.25F);
			this.AE_textBox_consoleInput.Location = new System.Drawing.Point(6, 486);
			this.AE_textBox_consoleInput.Multiline = true;
			this.AE_textBox_consoleInput.Name = "AE_textBox_consoleInput";
			this.AE_textBox_consoleInput.Size = new System.Drawing.Size(780, 35);
			this.AE_textBox_consoleInput.TabIndex = 1;
			this.AE_textBox_consoleInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AE_textBox_consoleInputKeyPress);
			// 
			// AE_textBox_consoleOutput
			// 
			this.AE_textBox_consoleOutput.Dock = System.Windows.Forms.DockStyle.Fill;
			this.AE_textBox_consoleOutput.Font = new System.Drawing.Font("Consolas", 8.25F);
			this.AE_textBox_consoleOutput.Location = new System.Drawing.Point(6, 318);
			this.AE_textBox_consoleOutput.Multiline = true;
			this.AE_textBox_consoleOutput.Name = "AE_textBox_consoleOutput";
			this.AE_textBox_consoleOutput.Size = new System.Drawing.Size(780, 162);
			this.AE_textBox_consoleOutput.TabIndex = 2;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(792, 573);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.Text = "DishAdvisor";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFormFormClosed);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.TextBox AE_textBox_consoleOutput;
		private System.Windows.Forms.TextBox AE_textBox_consoleInput;
		private System.Windows.Forms.WebBrowser AE_webBrowser_output;
		private System.Windows.Forms.ListBox AE_listBox_output;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem RedactViewToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem RedactBaseToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem RedactToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem OpenToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem CreateToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem FileToolStripMenuItem;
		private System.Windows.Forms.MenuStrip menuStrip1;
	}
}
