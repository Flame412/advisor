﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Tursukov A.E.
 * Дата: 28.05.2015
 * Время: 15:58
 * 
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using LexScanner;

namespace DishAdvisor
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		private bool _listen = true;
		private Thread _consoleThread;
		
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			AE_textBox_consoleOutput.AppendText(Solver.Phrases["hello"] + Environment.NewLine);
			AE_textBox_consoleOutput.AppendText(Solver.Phrases["initial"] + Environment.NewLine);
			_consoleThread = new Thread(ConsoleListener);
			_consoleThread.Start();
		}
		
		void ConsoleListener() {
			int count = 0;
			do {
				count = Messenger.Messages.Count;
				if (count > 0) {
					StringBuilder text = new StringBuilder("");
					for (int i = 0; i < count; i++) {
						text.AppendLine(Messenger.Messages[i]);
					}
					AE_textBox_consoleOutput.Invoke(
						(MethodInvoker) delegate { AE_textBox_consoleOutput.AppendText(text.ToString()); }
					);
					AE_textBox_consoleOutput.Invoke(
						(MethodInvoker) delegate { AE_textBox_consoleOutput.Update(); }
					);
					Messenger.Messages.Clear();
				}
				System.Threading.Thread.Sleep(100);
			} while(_listen);
		}
		
		void Solve(string input) {
			List<string> solved = Solver.Resolve(input);
			if (solved != null && solved.Count != 0) {
				AE_listBox_output.Items.Clear();
				foreach (string element in solved) {
					AE_listBox_output.Items.Add(element);
				}
			}
		}
		
		void DisplayHtml(string html) {
			AE_webBrowser_output.Navigate("about:blank");
			if (AE_webBrowser_output.Document != null) {
				AE_webBrowser_output.Document.Write(string.Empty);
			}
			AE_webBrowser_output.DocumentText = html;
		}
		
		void AboutToolStripMenuItemClick(object sender, EventArgs e)
		{
		}
		
		void AE_textBox_consoleInputKeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)Keys.Enter) {
				TextBox tbox = (TextBox) sender;
				string input = tbox.Text.TrimStart();
				Messenger.Messages.Add("> " + input);
				this.Solve(input);
				tbox.Text = "";
			}
		}
		
		void OpenToolStripMenuItemClick(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			DialogResult dResult = dialog.ShowDialog();
			if (dResult == DialogResult.OK) {
				string filename = dialog.FileName;
				KnowledgeBase.Load(filename);
			}
		}
		
		void MainFormFormClosed(object sender, FormClosedEventArgs e)
		{
			_listen = false;
		}
		
		void AE_listBox_outputSelectedIndexChanged(object sender, EventArgs e)
		{
			ListBox listBox = (ListBox) sender;
			string item = (string) listBox.SelectedItem;
			List<Rule> info = KnowledgeBase.FindInfo(item);
			string filename = info[0].Value;
			string dirUri = (new Uri(Assembly.GetExecutingAssembly().CodeBase)).LocalPath;
			string directory = Path.GetDirectoryName(dirUri);
			string path = String.Format("{0}\\bases\\views\\{1}", directory, filename);
			if (!File.Exists(path)) { throw new FileNotFoundException(); }
			using (StreamReader sr = File.OpenText(path)) {
				StringBuilder html = new StringBuilder("");
				while (sr.Peek() > -1) {
					html.Append(sr.ReadLine());
				}
				DisplayHtml(html.ToString());
			}
		}
		
		void RedactBaseToolStripMenuItemClick(object sender, EventArgs e)
		{
			BaseRedactor dialog = new BaseRedactor();
			dialog.Show();
		}
		
		void RedactViewToolStripMenuItemClick(object sender, EventArgs e)
		{
			ViewRedactor dialog = new ViewRedactor();
			dialog.Show();
		}
	}
}
