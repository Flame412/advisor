﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Tursukov A.E.
 * Дата: 28.05.2015
 * Время: 17:52
 * 
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using FastColoredTextBoxNS;

namespace DishAdvisor
{
	/// <summary>
	/// Description of ViewRedactor.
	/// </summary>
	public partial class ViewRedactor : Form
	{
		Dictionary<TabPage, string> _pagesNames;
		
		public ViewRedactor()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			_pagesNames = new Dictionary<TabPage, string>();
		}
		
		private void DisplayHtml(string html) {
			AE_webBrowser_output.Navigate("about:blank");
			if (AE_webBrowser_output.Document != null) {
				AE_webBrowser_output.Document.Write(string.Empty);
			}
			AE_webBrowser_output.DocumentText = html;
		}
		
		private FastColoredTextBox CreateTextBox(string text) {
			FastColoredTextBox result = new FastColoredTextBox();
			
			result.Dock = DockStyle.Fill;
			result.Text = text;
			result.TextChanged += FastColoredTextBox_TextChanged;
			result.OnTextChanged(0, text.Split('\n').Length - 1);
			
			return result;
		}
		
		private TabPage CreateTabPage(string name, string text) {
			TabPage result = new TabPage(name);
			
			result.Controls.Add(CreateTextBox(text));
			
			return result;
		}
		
		private void FastColoredTextBox_TextChanged(object sender, TextChangedEventArgs e) {
			FastColoredTextBox textBox = (FastColoredTextBox) sender;
			
			textBox.SyntaxHighlighter.HTMLSyntaxHighlight(e.ChangedRange);
			DisplayHtml(textBox.Text);
		}
		
		private string OpenHtml(string filename) {
			StringBuilder result = new StringBuilder("");
			using (StreamReader sr = new StreamReader(filename)) {
				while (sr.Peek() > -1) {
					result.AppendLine(sr.ReadLine());
				}
			}
			return result.ToString();
		}
		
		void AE_tabControl_editorsSelected(object sender, TabControlEventArgs e)
		{
			TabPage tabPage = (TabPage) sender;
			FastColoredTextBox textBox = (FastColoredTextBox) tabPage.Controls[0];
			DisplayHtml(textBox.Text);
		}
		
		void OpenToolStripMenuItemClick(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Filter = "HTML Document (*.html)|*.html";
			dialog.Multiselect = false;
			
			dialog.ShowDialog();
			string filename = dialog.FileName;
			string content = OpenHtml(filename);
			string name = Path.GetFileNameWithoutExtension(filename);
			TabPage tabPage = CreateTabPage(name, content);
			AE_tabControl_editors.TabPages.Add(tabPage);
			DisplayHtml(content);
			_pagesNames.Add(tabPage, filename);
		}
		
		void SaveToolStripMenuItemClick(object sender, EventArgs e)
		{
			TabPage tabPage = AE_tabControl_editors.SelectedTab;
			FastColoredTextBox textBox = (FastColoredTextBox) tabPage.Controls[0];
			if (_pagesNames.ContainsKey(tabPage)) {
				File.WriteAllText(_pagesNames[tabPage], textBox.Text);
			} else {
				SaveFileDialog dialog = new SaveFileDialog();
				dialog.Filter = "HTML Document (*.html)|*.html";
				dialog.ShowDialog();
				string filename = dialog.FileName;
				File.WriteAllText(filename, textBox.Text);
				tabPage.Name = Path.GetFileNameWithoutExtension(filename);
				_pagesNames.Add(tabPage, filename);
			}
		}
		
		void CloseToolStripMenuItemClick(object sender, EventArgs e)
		{
			this.Close();
		}
		
		void CreateToolStripMenuItemClick(object sender, EventArgs e)
		{
			AE_tabControl_editors.TabPages.Add(CreateTabPage("untitled", string.Empty));
		}
	}
}
