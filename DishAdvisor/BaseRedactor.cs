﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Tursukov A.E.
 * Дата: 28.05.2015
 * Время: 17:52
 * 
 */
using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Collections.Generic;
using FastColoredTextBoxNS;
using NUnit.Framework;

namespace DishAdvisor
{
	/// <summary>
	/// Description of BaseRedactor.
	/// </summary>
	public partial class BaseRedactor : Form
	{
		private Style _blueStyle = new TextStyle(Brushes.Blue, null, FontStyle.Regular);
		private Style _purpleStyle = new TextStyle(Brushes.Purple, null, FontStyle.Italic);
		private Dictionary<TabPage, string> _pagesNames;
		
		public BaseRedactor()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			_pagesNames = new Dictionary<TabPage, string>();
			
			List<string> files = KnowledgeBase.Files;
			if (files.Count > 0) {
				foreach (string file in files) {
					StringBuilder text = new StringBuilder("");
					using (StreamReader sr = File.OpenText(file)) {
						while (sr.Peek() > -1) {
							text.AppendLine(sr.ReadLine());
						}
					}
					
					string name = Path.GetFileNameWithoutExtension(file);
					TabPage tabPage = CreateTabPage(name, text.ToString());
					AE_tabControl_editors.TabPages.Add(tabPage);
					_pagesNames.Add(tabPage, file);
				}
			} else {
				AE_tabControl_editors.TabPages.Add(CreateTabPage("untitled1", string.Empty));
			}
		}
		
		private FastColoredTextBox CreateTextBox(string text) {
			FastColoredTextBox result = new FastColoredTextBox();
			
			result.Dock = DockStyle.Fill;
			result.Text = text;
			result.TextChanged += FastColoredTextBox_TextChanged;
			result.AddStyle(_blueStyle);
			result.AddStyle(_purpleStyle);
			result.OnTextChanged();
			
			return result;
		}
		
		private TabPage CreateTabPage(string name, string text) {
			TabPage result = new TabPage(name);
			
			result.Controls.Add(CreateTextBox(text));
			
			return result;
		}
		
		private void FastColoredTextBox_TextChanged(object sender, TextChangedEventArgs e) {
			FastColoredTextBox textBox = (FastColoredTextBox) sender;
						
			e.ChangedRange.ClearStyle(_blueStyle);
			e.ChangedRange.SetStyle(_blueStyle, @"[Ii]nformation|[Oo]bject|[Rr]eturn", RegexOptions.Multiline);
				
			e.ChangedRange.ClearStyle(_purpleStyle);
			e.ChangedRange.SetStyle(_purpleStyle, @"(?<=\()[^\(\)]*(?=\))", RegexOptions.Multiline);
		}
		
		void SaveToolStripMenuItemClick(object sender, EventArgs e)
		{
			TabPage tabPage = AE_tabControl_editors.SelectedTab;
			FastColoredTextBox textBox = (FastColoredTextBox) tabPage.Controls[0];
			string filename = "";
			string text = textBox.Text;
			if (_pagesNames.ContainsKey(tabPage)) {
				filename = _pagesNames[tabPage];
			} else {
				SaveFileDialog dialog = new SaveFileDialog();
				dialog.Filter = "Advisor base (*.base)|*.base";
				dialog.ShowDialog();
				filename = dialog.FileName;
				_pagesNames.Add(tabPage, filename);
				tabPage.Name = Path.GetFileNameWithoutExtension(filename);
			}
			KnowledgeBase.Save(filename, text);
		}
		
		private string OpenBase(string filename) {
			StringBuilder result = new StringBuilder("");
			
			using (StreamReader sr = new StreamReader(filename)) {
				while (sr.Peek() > -1) {
					result.AppendLine(sr.ReadLine());
				}
			}
			
			return result.ToString();
		}
		
		void OpenToolStripMenuItemClick(object sender, EventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Filter = "Advisor base (*.base)|*.base";
			dialog.Multiselect = false;
			
			dialog.ShowDialog();
			string filename = dialog.FileName;
			string content = OpenBase(filename);
			string name = Path.GetFileNameWithoutExtension(filename);
			TabPage tabPage = CreateTabPage(name, content);
			AE_tabControl_editors.TabPages.Add(tabPage);
			_pagesNames.Add(tabPage, filename);
		}
		
		void CreateToolStripMenuItemClick(object sender, EventArgs e)
		{
			AE_tabControl_editors.TabPages.Add(CreateTabPage("untitled", string.Empty));
		}
		
		void CloseToolStripMenuItemClick(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
