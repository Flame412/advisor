﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Tursukov A.E.
 * Дата: 29.05.2015
 * Время: 19:41
 * 
 */
using System;

namespace DishAdvisor
{
	/// <summary>
	/// Description of Rule.
	/// </summary>
	public class Rule
	{
		public enum RuleType {
			Unknown = -1,
			Object = 0,
			Info = 1
		}
		
		private String _name;
		private String _value;
		private RuleType _type;
		
		#region get/set
		public string Name {
			get { return _name; }
			set { _name = value; }
		}
		
		public string Value {
			get { return _value; }
			set { _value = value; }
		}
		
		public Rule.RuleType Type {
			get { return _type; }
			set { _type = value; }
		}
		#endregion
		
		public Rule(String name, String value, RuleType type)
		{
			_name = name;
			_value = value;
			_type = type;
		}
		
		public String TypeToString(RuleType type) {
			switch (type) {
				case RuleType.Object: 
					return "Object";
				case RuleType.Info: 
					return "Info";
				default:
					return null;
			}
		}
	}
}
