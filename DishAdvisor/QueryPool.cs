﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Tursukov A.E.
 * Дата: 08.06.2015
 * Время: 13:30
 * 
 */
using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;

namespace DishAdvisor
{
	/// <summary>
	/// Description of QueryPool.
	/// </summary>
	public class QueryPool
	{
		//TODO: probably query class should know which facts doubles were reduced
		public class Query : ICloneable
		{
			private List<string> _facts;
			
			public List<string> Facts {
				get { return _facts; }
			}
			
			public Query() {
				_facts = new List<string>();
			}
			
			public object Clone() {
				Query result = (Query)this.MemberwiseClone();
				
				result._facts = new List<string>(_facts);
				
				return result;
			}
		}
		
		private List<Query> _queries;
		private List<string> _doublingDelayed;
		
		public List<QueryPool.Query> Queries {
			get { return _queries; }
		}
		
		public QueryPool() {
			_queries = new List<QueryPool.Query>();
			_doublingDelayed = new List<string>();
		}
		
		private void Add(string input, IList<Query> collection) {
			int count = _queries.Count;
			if(count < 1) {
				_queries.Add(new Query());
			}
			count = _queries.Count;
			for(int i = 0; i < count; i++) {
				collection[i].Facts.Add(input);
			}
		}
		
		public void Add(string input) {
			Add(input, _queries);
		}
		
		public void AddDoubling(string first, string second) {
			AddDoubling(first, new List<string>() {second});
		}
		
		public void AddDoubling(string first, IList<string> second) {
			List<List<Query>> right = new List<List<QueryPool.Query>>();
			int count = _queries.Count;
			foreach (string element in second) {
				List<Query> twin = new List<Query>(count);
				_queries.ForEach((item) => {
				                 	twin.Add((Query)item.Clone());
				                 });
				Add(element, twin);
				right.Add(twin);
			}
			Add(first);
			foreach (List<Query> twin in right) {
				_queries.AddRange(twin);
			}
		}
		
		public void AddDoublingWithDelay(string input) {
			_doublingDelayed.Add(input);
		}
		
		public void ResolveDelayedDoubling() {
			string first = _doublingDelayed[0];
			_doublingDelayed.RemoveAt(0);
			AddDoubling(first, _doublingDelayed);
			
			_doublingDelayed.Clear();
		}
	}
}
