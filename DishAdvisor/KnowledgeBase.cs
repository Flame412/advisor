﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Tursukov A.E.
 * Дата: 28.05.2015
 * Время: 23:01
 * 
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;

namespace DishAdvisor
{
	/// <summary>
	/// Description of KnowledgeBase.
	/// </summary>
	public static class KnowledgeBase
	{
		private static List<string> _files = new List<string>();
		private static List<Rule> _rules = new List<Rule>();
		private static Dictionary<string, Regex> _regularExpressions 
			= new Dictionary<string, Regex>() {
			{ "Info", new Regex(@"^[Ii]nformation\([\w\s]*\)\s*:-\s*[Rr]eturn\([^\(\),]*\)\.?$") },
			{ "Object", new Regex(@"^[Oo]bject\([\w\s]*,[\w\s]*\)\.?$") }
		};
		private static Dictionary<string, string> _phrases = new Dictionary<string, string>() {
			{ "load-success", "Загружена база {0}" },
			{ "load-fail", "Не удалось загрузить базу {0}" },
			{ "read-line-fail", "Не удалось распознать строку {0}" }
		};
		
		#region get/set
		public static List<string> Files {
			get { return _files; }
			set { _files = value; }
		}
		
		public static List<Rule> Rules {
			get { return _rules; }
			set { _rules = value; }
		}
		#endregion
		
		public static bool Create(string filename) {
			bool result = false;
			
			try {
				File.Create(filename).Close();
				result = true;
				_files.Add(filename);
			} catch (Exception) {
				throw;
			}
			
			return result;
		}
		
		//TODO: проверка на существование, открыт ли
		public static bool Load(string filename) {
			bool result = false;
			
			try {
				using(StreamReader sr = new StreamReader(filename, Encoding.UTF8)) {
					string line;
					Rule rule;
					int lineno = 1;
					while((line = sr.ReadLine()) != null) {
						rule = Parse(line);
						if (rule != null) { 
							_rules.Add(rule); 
						} else {
							if (!String.IsNullOrEmpty(line)) {
								Messenger.Messages.Add(String.Format(_phrases["read-line-fail"], line));
							}
						}
						lineno++;
					}
				}
				
				_files.Add(filename);
				Messenger.Messages.Add(
					String.Format(_phrases["load-success"], filename)
				);
				result = true;
			} catch (Exception) {
				Messenger.Messages.Add(
					String.Format(_phrases["load-failure"], filename)
				);
				throw;
			}
			
			return result;
		}
		
		public static bool Save(string filename, string content) {
			bool result = false;
			
			using (FileStream fs = File.Create(filename)) {
				try {
					Encoding encoding = new UTF8Encoding();
					byte[] buffer = encoding.GetBytes(content);
					fs.Write(buffer, 0, buffer.Length);
					result = true;
				} catch (Exception) {
					throw;
				}
			}
			
			return result;
		}
		
		public static Rule FindObject(string name, string fact) {
			Rule result = null;
			
			result = _rules.Find(
				rule => { 
					return rule.Name == name 
						&& rule.Value == fact
						&& rule.Type == Rule.RuleType.Object;
				}
			);
			
			return result;
		}
		
		public static List<Rule> FindAllObjects(IList<string> facts) {
			List<Rule> result = null;
			
			result = _rules
				.Where(rule => facts.Contains(rule.Value) && rule.Type == Rule.RuleType.Object)
				.GroupBy(rule => rule.Name)
				.Select(rule => rule.First())
				.ToList();
			
			return result;
		}
		
		public static List<Rule> FindInfo(string name) {
			List<Rule> result = null;
			
			result = _rules.FindAll(
				rule => { 
					return rule.Name == name
						&& rule.Type == Rule.RuleType.Info;
				}
			);
			
			return result;
		}
		
		public static List<Rule> FindFacts(string name) {
			List<Rule> result = null;
			
			result = _rules.FindAll(
				rule => {
					return rule.Name == name
						&& rule.Type == Rule.RuleType.Object;
				}
			);
			
			return result;
		}
		
		public static Rule Parse(string line) {
			Rule result = null;
			
			Rule.RuleType type = Rule.RuleType.Unknown;
			
			Regex pattern;
			if (_regularExpressions["Object"].IsMatch(line)) {
				type = Rule.RuleType.Object;
				pattern = new Regex(@"\([\w\s]*,[\w\s]*\)");
			} else if (_regularExpressions["Info"].IsMatch(line)) {
				type = Rule.RuleType.Info;
				pattern = new Regex(@"\([^\(\)]*\)");
			} else {
				return result;
			}
			
			MatchCollection matching = pattern.Matches(line);
			string name, val;
			char []braces = new char[] {'(', ')'};
			if (matching.Count == 1) {
				string tmp = matching[0].Value.Trim(braces);
				string []tmpArr = tmp.Split(',');
				name = tmpArr[0].Trim();
				val = tmpArr[1].Trim();
			} else if (matching.Count == 2) {
				name = matching[0].Value.Trim(braces);
				val = matching[1].Value.Trim(braces);
			} else {
				return result;
			}
			
			result = new Rule(name, val, type);
			
			return result;
		}
	}
}
