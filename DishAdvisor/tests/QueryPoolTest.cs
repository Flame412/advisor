﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Tursukov A.E.
 * Дата: 08.06.2015
 * Время: 14:10
 * 
 */
using System;
using NUnit.Framework;

namespace DishAdvisor.tests
{
	[TestFixture]
	public class QueryPoolTest
	{
		QueryPool pool;
		
		[SetUp]
		public void Init() {
			pool = new QueryPool();
		}
		
		[Test]
		public void Add() {
			string input = "item1";
			pool.Add(input);
			Assert.AreEqual(1, pool.Queries.Count, "query count should be 1");
			Assert.IsTrue(pool.Queries[0].Facts.Contains(input), "query should contain <item1>");
		}
		
		[Test]
		public void AddDoubling() {
			string input = "item1";
			pool.Add(input);
			pool.AddDoubling("item21", "item22");
			Assert.AreEqual(2, pool.Queries.Count, "query count should be 2");
			Assert.IsTrue(pool.Queries[0].Facts.Contains(input), "query[0] should contain <item1>");
			Assert.IsTrue(pool.Queries[0].Facts.Contains("item21"), "query[0] should contain <item21>");
			Assert.IsFalse(pool.Queries[0].Facts.Contains("item22"), "query[0] should not contain <item22>");
			
			Assert.IsTrue(pool.Queries[1].Facts.Contains(input), "query[1] should contain <item1>");
			Assert.IsTrue(pool.Queries[1].Facts.Contains("item22"), "query[1] should contain <item22>");
			Assert.IsFalse(pool.Queries[1].Facts.Contains("item21"), "query[1] should not contain <item21>");
		}
	}
}
