﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Tursukov A.E.
 * Дата: 05.06.2015
 * Время: 17:24
 * 
 */
using System;
using LexScanner;
using NUnit.Framework;

namespace DishAdvisor.tests
{
	[TestFixture]
	public class ParserTest
	{
		Scanner scanner;
		Parser parser;
		
		[SetUp]
		public void Init() {
			scanner = new Scanner();
			parser = new Parser(scanner);
		}
		
		[Test]
		public void And()
		{
			String input = "qwe, asd.";
			scanner.SetSource(input, 0);
			Assert.IsTrue(parser.Parse());
			Node root = parser.Root;
			Assert.IsNull(root.Right);
			Assert.AreEqual(NodeType.AND, root.Left.Type);
			Assert.AreEqual("qwe", root.Left.Left.sVal);
			Assert.AreEqual("asd", root.Left.Right.sVal);
		}
		
		[Test]
		public void Or() {
			String input = "asd; qwe.";
			scanner.SetSource(input, 0);
			Assert.IsTrue(parser.Parse());
			Node root = parser.Root;
			Assert.IsNull(root.Right);
			Assert.AreEqual(NodeType.OR, root.Left.Type);
			Assert.AreEqual("asd", root.Left.Left.sVal);
			Assert.AreEqual("qwe", root.Left.Right.sVal);
		}
		
		[Test]
		public void ParseSeveralWords() {
			String input = "asd zxc; qwe.";
			scanner.SetSource(input, 0);
			Assert.IsTrue(parser.Parse());
			Node root = parser.Root;
			Assert.IsNull(root.Right);
			Assert.AreEqual(NodeType.OR, root.Left.Type);
			Assert.AreEqual("asd zxc", root.Left.Left.sVal);
			Assert.AreEqual("qwe", root.Left.Right.sVal);
		}
	}
}
