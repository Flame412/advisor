﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Tursukov A.E.
 * Дата: 05.06.2015
 * Время: 12:30
 * 
 */
using System;
using System.IO;
using System.Collections.Generic;
using NUnit.Framework;

namespace DishAdvisor.tests
{
	[TestFixture]
	public class KnowledgeBaseTest
	{		
		[Test]
		public void Create()
		{
			string filename = Path.GetRandomFileName();
			Assert.IsTrue(KnowledgeBase.Create(filename));
			Assert.IsTrue(File.Exists(filename), "file not created");
			Assert.Contains(filename, KnowledgeBase.Files);
			Assert.AreEqual(1,KnowledgeBase.Files.Count);
			KnowledgeBase.Files.Remove(filename);
			File.Delete(filename);
		}
		
		[Test]
		public void Load() {
			string filename = Path.GetRandomFileName();
			File.Create(filename).Close();
			Assert.IsTrue(KnowledgeBase.Load(filename));
			Assert.Contains(filename, KnowledgeBase.Files);
			Assert.AreEqual(1,KnowledgeBase.Files.Count);
			KnowledgeBase.Files.Remove(filename);
			File.Delete(filename);
		}
		
		[Test]
		public void Save() {
			string filename = Path.GetRandomFileName();
			string content = "Bad wolf.";
			Assert.IsTrue(KnowledgeBase.Save(filename, content));
			Assert.IsTrue(File.Exists(filename));
			using(StreamReader sr = File.OpenText(filename)) {
				string text = sr.ReadToEnd();
				Assert.AreEqual(content, text);
			}
			File.Delete(filename);
		}
		
		[Test]
		public void FindObject() {
			string name = "Wolf";
			string fact = "Girl";
			string input = String.Format("object({0}, {1}).", name, fact);
			Rule rule = KnowledgeBase.Parse(input);
			Assert.IsNotNull(rule);
			KnowledgeBase.Rules.Add(rule);
			KnowledgeBase.FindObject(name, fact);
		}
		
		[Test]
		public void FindAllObjects() {
			KnowledgeBase.Rules.Add(KnowledgeBase.Parse("object(Witcher, drama)."));
			KnowledgeBase.Rules.Add(KnowledgeBase.Parse("object(Witcher, fantasy)."));
			KnowledgeBase.Rules.Add(KnowledgeBase.Parse("object(Witcher, action)."));
			
			KnowledgeBase.Rules.Add(KnowledgeBase.Parse("object(Amber, drama)."));
			KnowledgeBase.Rules.Add(KnowledgeBase.Parse("object(Amber, fantasy)."));
			KnowledgeBase.Rules.Add(KnowledgeBase.Parse("object(Amber, action)."));
			KnowledgeBase.Rules.Add(KnowledgeBase.Parse("object(Amber, other worlds)."));
			
			KnowledgeBase.Rules.Add(KnowledgeBase.Parse("object(Wild sheep hunt, mystic)."));
			KnowledgeBase.Rules.Add(KnowledgeBase.Parse("object(Wild sheep hunt, adventures)."));
			
			string []facts = new string[] {
				"drama", "fantasy", "action"
			};
			List<Rule> rules = KnowledgeBase.FindAllObjects(facts);
			Assert.IsNotEmpty(rules);
			Assert.AreEqual(2, rules.Count);
		}
		
		[Test]
		public void FindInfo() {
			KnowledgeBase.Rules.Add(KnowledgeBase.Parse("information(Witcher) :- return(Sapkovsky)."));
			KnowledgeBase.Rules.Add(KnowledgeBase.Parse("information(Witcher) :- return(2005)."));
			
			string name = "Witcher";
			List<Rule> rules = KnowledgeBase.FindInfo(name);
			Assert.AreEqual(2, rules.Count);
		}
		
		[Test]
		public void ParseSimpleObject() {
			String line = "object(qwe, asd)";
			Rule rule = KnowledgeBase.Parse(line);
			Assert.IsNotNull(rule, "line " + line + "should parse correctly");
			Assert.AreEqual(Rule.RuleType.Object, rule.Type, "rule should be of type Object");
			Assert.AreEqual("qwe", rule.Name, "rule name should be equal to <qwe>");
			Assert.AreEqual("asd", rule.Value, "rule value should be equal to <asd>");
		}
		
		[Test]
		public void ParseObjectWithNameOfTwoWords() {
			String line = "object(qwe asd, zxc)";
			Rule rule = KnowledgeBase.Parse(line);
			Assert.IsNotNull(rule, "line " + line + "should parse correctly", line);
			Assert.AreEqual(Rule.RuleType.Object, rule.Type, "rule should be of type Object");
			Assert.AreEqual("qwe asd", rule.Name, "rule name should be equal to <qwe asd>");
			Assert.AreEqual("zxc", rule.Value, "rule value should be equal to <zxc>");
		}
		
		[Test]
		public void ParseObjectWithDot() {
			String line = "object(asd, asd).";
			Rule rule = KnowledgeBase.Parse(line);
			Assert.IsNotNull(rule, "line " + line + "should parse correctly", line);
			Assert.AreEqual(Rule.RuleType.Object, rule.Type, "rule should be of type Object");
		}
		
		[Test]
		public void ParseIncorrectObjectShouldFail() {
			String line = "object(asd, asd, asd).";
			Rule rule = KnowledgeBase.Parse(line);
			Assert.IsNull(rule, "line" + line + "should not parse", line);
			
			line = "object(asd, asd.";
			rule = KnowledgeBase.Parse(line);
			Assert.IsNull(rule, "line" + line + "should not parse", line);
			
			line = "objectasd, asd).";
			rule = KnowledgeBase.Parse(line);
			Assert.IsNull(rule, "line" + line + "should not parse", line);
			
			line = "object(asd).";
			rule = KnowledgeBase.Parse(line);
			Assert.IsNull(rule, "line" + line + "should not parse", line);
		}
		
		[Test]
		public void ParseSimpleInfo() {
			String line = "information(qwe):- return(asd)";
			Rule rule = KnowledgeBase.Parse(line);
			Assert.IsNotNull(rule, "line " + line + "should parse correctly");
			Assert.AreEqual(Rule.RuleType.Info, rule.Type, "rule should be of type Info");
			Assert.AreEqual("qwe", rule.Name, "rule name should be equal to <qwe>");
			Assert.AreEqual("asd", rule.Value, "rule value should be equal to <asd>");
		}
		
		[Test]
		public void ParseInfoWithNameOfTwoWords() {
			String line = "Information(qwe asd) :- Return(zxc)";
			Rule rule = KnowledgeBase.Parse(line);
			Assert.IsNotNull(rule, "line " + line + "should parse correctly", line);
			Assert.AreEqual(Rule.RuleType.Info, rule.Type, "rule should be of type Info");
			Assert.AreEqual("qwe asd", rule.Name, "rule name should be equal to <qwe asd>");
			Assert.AreEqual("zxc", rule.Value, "rule value should be equal to <zxc>");
		}
		
		[Test]
		public void ParseInfoWithDot() {
			String line = "information(asd) :-   Return(asd).";
			Rule rule = KnowledgeBase.Parse(line);
			Assert.IsNotNull(rule, "line " + line + "should parse correctly", line);
			Assert.AreEqual(Rule.RuleType.Info, rule.Type, "rule should be of type Info");
		}
		
		[Test]
		public void ParseIncorrectInfoShouldFail() {
			String line = "information(asd, asd) :- return(asd).";
			Rule rule = KnowledgeBase.Parse(line);
			Assert.IsNull(rule, "line" + line + "should not parse", line);
			
			line = "information(asd) :- return(asd, asd).";
			rule = KnowledgeBase.Parse(line);
			Assert.IsNull(rule, "line" + line + "should not parse", line);
			
			line = "information(asd).";
			rule = KnowledgeBase.Parse(line);
			Assert.IsNull(rule, "line" + line + "should not parse", line);
		}
	}
}
