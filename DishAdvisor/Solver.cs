﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Tursukov A.E.
 * Дата: 03.06.2015
 * Время: 18:25
 * 
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using LexScanner;

namespace DishAdvisor
{
	/// <summary>
	/// Description of Solver.
	/// </summary>
	public static class Solver
	{
		private static int _state = INITIAL;

		private const int INITIAL = 0;
		private const int FORWARD = 1;
		private const int BACKWARD = 2;
		
		private static Dictionary<String, String> _phrases = 
			new Dictionary<String, String>() {
			{"hello", "Здравствуйте."},
			{"initial", "Вы знаете наименование искомого блюда? (Yes/No)"},
			{"unknown", "Не могу понять."},
			{"template", "Воспользуйтесь, пожалуйста, одной из следующих форм:"},
			{"facts", "Введите, пожалуйста, список ингредиентов"},
			{"cancel", "Или введите «cancel» для возвращения к предыдущему вопросу."},
			{"yesno-tpl", "Yes/No"},
			{"facts-tpl", "ингредиент1, ингредиент21 или ингредиент22, ингредиент3, ... ."},
			{"name", "Введите, пожалуйста, название искомого блюда."},
			{"count", "Найдено {0} объектов"},
			{"dot", "Возможно вы забыли поставить точку в конце."},
			{"state", "Переход в состояние {0}"},
			{"state-initial", "начальное"},
			{"state-forward", "прямого вывода"},
			{"state-backward", "обратного вывода"}
		};
		
		private static Dictionary<String, Regex> _templates =
			new Dictionary<String, Regex>() {
			{"yes", new Regex("([Yy]es)\\.?")},
			{"no", new Regex("([Nn]o)\\.?")},
			{"cancel", new Regex("([Cc]ancel)\\.?")}
		};
		
		private static Scanner _scanner = new Scanner(); //We'll initialize source later
		private static Parser _parser = new Parser(_scanner);
		
		#region get/set
		public static Dictionary<string, string> Phrases {
			get { return _phrases; }
		}
		#endregion
		
		public static List<string> Resolve(String input) {
			List<string> result = null;
			
#if TRACE_ACTIONS
			AttachConsole(-1);
			AllocConsole();
#endif
			
			_scanner.SetSource(input, 0);	//Initialize scanner source.
			bool parsed = _parser.Parse();
			List<QueryPool> poolList = new List<QueryPool>();
			Node root = null;
			if (parsed) {
				 root = _parser.Root;
				poolList = ParseTreeHelper.ParseTree(root);
			}
			if (poolList.Count != 0) {
				List<Rule> rules = new List<Rule>();
				switch (_state) {
					case INITIAL:
						#region INITIAL
						string fact = poolList[0].Queries[0].Facts[0];
						if (_templates["yes"].IsMatch(fact)) {
							_state = BACKWARD;
							
							Messenger.Messages.Add(String.Format(_phrases["state"], _phrases["state-backward"]));
							Messenger.Messages.Add(_phrases["name"]);
							Messenger.Messages.Add(_phrases["cancel"]);
						} else if (_templates["no"].IsMatch(fact)) {
							_state = FORWARD;
							
							Messenger.Messages.Add(String.Format(_phrases["state"], _phrases["state-forward"]));
							Messenger.Messages.Add(_phrases["facts"]);
							Messenger.Messages.Add(_phrases["cancel"]);
						} else {
							Messenger.Messages.Add(_phrases["unknown"]);
							Messenger.Messages.Add(_phrases["template"]);
							Messenger.Messages.Add(_phrases["yesno-tpl"]);
							return result;
						}
						#endregion
						break;
					case FORWARD:
						#region FORWARD
						if (_templates["cancel"].IsMatch(input)) {
							_state = INITIAL;
							Messenger.Messages.Add(String.Format(_phrases["state"], _phrases["state-initial"]));
							Messenger.Messages.Add(_phrases["initial"]);
							return result;
						}
						
						foreach (QueryPool element in poolList) {
							foreach (QueryPool.Query item in element.Queries) {
								rules.AddRange(KnowledgeBase.FindAllObjects(item.Facts));
							}
						}
						result = new List<string>();
						rules.ForEach(rule => {
						              	result.Add(rule.Name);
						              });
						Messenger.Messages.Add(String.Format(_phrases["count"], result.Count));
						#endregion
						break;
					case BACKWARD:
						#region BACKWARD
						if (_templates["cancel"].IsMatch(input)) {
							_state = INITIAL;
							Messenger.Messages.Add(String.Format(_phrases["state"], _phrases["state-initial"]));
							Messenger.Messages.Add(_phrases["initial"]);
							return result;
						}
						string name = "";
						foreach (QueryPool element in poolList) {
							foreach (QueryPool.Query item in element.Queries) {
								name = item.Facts[0];
								rules.AddRange( KnowledgeBase.FindInfo(name));
							}
						}
						result = new List<string>();
						rules.ForEach(rule => {
						              	result.Add(rule.Name);
						              });
						StringBuilder res = new StringBuilder("");
						List<Rule> facts = KnowledgeBase.FindFacts(name);
						facts.ForEach(rule => {
						              	res.Append(rule.Value + ", ");
						              });
						Messenger.Messages.Add(res.ToString().TrimEnd(new char[]{',', ' '}));
						#endregion
						break;
					default:
						throw new NotImplementedException();
				}
				
			} else {
				Messenger.Messages.Add(_phrases["unknown"]);
				Messenger.Messages.Add(_phrases["dot"]);
				Messenger.Messages.Add(_phrases["template"]);
				Messenger.Messages.Add(_phrases["facts-tpl"]);
			}
			return result;
		}
		
		
#if TRACE_ACTIONS		
	    [System.Runtime.InteropServices.DllImport("kernel32.dll")]
	    private static extern bool AllocConsole();
	
	    [System.Runtime.InteropServices.DllImport("kernel32.dll")]
	    private static extern bool AttachConsole(int pid);
#endif
	}
}
